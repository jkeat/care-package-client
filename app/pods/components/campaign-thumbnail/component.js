import Component from '@ember/component';
import { computed } from '@ember/object';
import _ from 'npm:lodash';
import moment from 'npm:moment';

export default Component.extend({
  classNames: ['campaign'],

  percent: computed('campaign.{priceNano,raisedNano}', function () {
    return _.round(this.get('campaign.raisedNano') * 100 / this.get('campaign.priceNano'));
  }),

  remaining: computed('campaign.timeLeft', function () {
    return moment().add(this.get('campaign.timeLeft'), 'milliseconds').fromNow(true);
  }),
});
