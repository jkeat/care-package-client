import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  address: null,
  amount: null,
  token: null,

  transactions: service(),

  uuid: computed('address', function () {
    return `brain-${this.get('address')}`;
  }),

  transaction: computed('address', 'amount', 'token', function () {
    return {
      to: this.get('address'),
      amount: this.get('amount'),
      token: this.get('token'),
    }
  }),

  setup() {
    brainblocks.Button.render({
      // Pass in payment options
      payment: {
        destination: this.get('address'),
        currency:    'rai',
        amount:      this.get('amount') * 1000000,
      },

      // Handle successful payments
      onPayment: function(data) {
        if (!window.nanoHistory) {
          window.nanoHistory = [];
        }
        window.nanoHistory.push(data);

        this.set('token', data.token);
        const transaction = this.get('transaction');
        this.get('transactions').submitTransaction(transaction);
      }.bind(this)
    }, this.get('uuid'));
  },

  didInsertElement() {
    this._super();

    this.setup();
  },
});
