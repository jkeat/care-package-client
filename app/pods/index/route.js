import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  campaigns: service(),

  model() {
    return this.get('campaigns').getCampaigns();
  },
});
