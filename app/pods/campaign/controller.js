import Controller from '@ember/controller';
import { computed } from '@ember/object';
import moment from 'npm:moment';
import _ from 'npm:lodash';

export default Controller.extend({
  status: computed('isActive', 'isFunded', function () {
    const isActive = this.get('model.isActive');
    const isFunded = this.get('model.isFunded');

    if (isActive && !isFunded) {
      return {
        msg: 'Funding',
        flag: 'funding',
      };
    }

    if (!isActive && !isFunded) {
      return {
        msg: 'Failed to fund',
        flag: 'failed',
      };
    }

    return {
      msg: 'Successfully funded',
      flag: 'funded',
    };
  }),

  remaining: computed('model.timeLeft', function () {
    return moment().add(this.get('model.timeLeft'), 'milliseconds').fromNow(true);
  }),

  percent: computed('model.{priceNano,raisedNano}', function () {
    return _.round(this.get('model.raisedNano') * 100 / this.get('model.priceNano'));
  }),

  donationAmount: 0,
  brainGiven: false,

  actions: {
    giveBrain() {
      this.set('brainGiven', true);
    },
  },
});
