import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  campaigns: service(),

  model(params) {
    return this.get('campaigns').find(params.campaign_id);
  },

  resetController(controller) {
    controller.set('brainGiven', false);
    controller.set('donationAmount', 0);
  },
});
