import Route from '@ember/routing/route';

export default Route.extend({
  resetController(controller) {
    controller.set('brainGiven', false);
    controller.set('donationAmount', 0);

    controller.setProperties({
      amazonLink: '',
      person: '',
      organization: '',
      streetAddress: '',
      plea: '',
      submitted: false,
    })
  },
});
