import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
  campaigns: service(),

  amazonLink: '',
  person: '',
  organization: '',
  streetAddress: '',
  plea: '',

  submitted: false,

  formObj: computed('amazonLink', 'person', 'organization', 'streetAddress', 'plea', function () {
    return {
      amazonLink: this.get('amazonLink'),
      person: this.get('person'),
      organization: this.get('organization'),
      streetAddress: this.get('streetAddress'),
      plea: this.get('plea'),
    };
  }),

  actions: {
    submit() {
      this.get('campaigns').submitCampaign(this.get('formObj'))
        .then((campaign) => {
          this.set('submitted', true);

          this.set('campaign', campaign);
        });
    },
  },
});
