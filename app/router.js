import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('campaign', { path: '/:campaign_id' });
  this.route('create-campaign', { path: '/new' });
  this.route('transactions');
});

export default Router;
