import Service from '@ember/service';
import ENV from 'neodrop/config/environment';
import fetch from 'fetch';

export default Service.extend({
  submitTransaction(transaction) {
    return fetch(`${ENV.APP.apiBase}/got_donation/`, {
      method: 'POST',
      body:    JSON.stringify(transaction),
      headers: { 'Content-Type': 'application/json' },
    });
  },
});
