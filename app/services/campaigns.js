import Service from '@ember/service';
// import { data } from 'neodrop/utils/dummy-data';
import ENV from 'neodrop/config/environment';
import _ from 'npm:lodash';
import fetch from 'fetch';

export default Service.extend({
  campaigns: null,

  getCampaigns() {
    if (this.get('campaigns')) {
      return Promise.resolve(this.get('campaigns'));
    }

    return fetch(`${ENV.APP.apiBase}/getposts`)
      .then((res) => res.json());
  },

  addCampaign(campaign) {
    return this.getCampaigns().then((campaigns) => {
      campaigns.pushObject(campaign);
    });
  },

  submitCampaign(campaign) {
    return fetch(`${ENV.APP.apiBase}/createpost/`, {
      method: 'POST',
      body:    JSON.stringify(campaign),
      headers: { 'Content-Type': 'application/json' },
    }).then((res) => res.json()).then((campaign) => {
      return this.addCampaign(campaign).then(() => campaign);
    });
  },

  find(id) {
    return this.getCampaigns().then((campaigns) => {
      return _.find(campaigns, { post_id: id });
    });
  },
});
